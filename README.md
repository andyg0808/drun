## Introduction
`drun` simplifies the command line needed to run a Docker image with an arbitrary entrypoint:

```
docker run -v$(pwd):/mnt -w/mnt -it --entrypoint="bash" python -c "pip install mypy && mypy drun"
```
becomes
```
drun python bash -c "pip install mypy && mypy drun"
```
With the companion `dsh`, this can be simplified further:
```
dsh python "pip install mypy && mypy drun"
```


## Usage
### `drun`
```
drun [dockeropts] image entrypoint [entrypointopts]
```

Runs `entrypoint` in `image`, passing `dockeropts` to the `docker run`
command and `entrypointopts` to the `entrypoint` command.

<dl>
<dt><tt>dockeropts</tt></dt>
<dd>Any arguments accepted by <tt>docker run</tt>.</dd>
<dt><tt>image</tt></dt>
<dd>The name of a Docker image to run</dd>
<dt><tt>entrypoint</tt></dt>
<dd>The entrypoint to use in that image</dd>
<dt><tt>entrypointopts</tt></dt>
<dd>Options to pass to <tt>entrypoint</tt></dd>

### `dsh`
```
dsh [dockeropts] image [shargs] [shcommand] [commandargs...]
```

Runs `shcommand` via `sh` in `image`, passing `dockeropts` to the `docker run`
command, `shargs` to `sh`, and any `commandargs` to `shcommand` within
`sh`.

If `shcommand` is provided without any further `commandargs`, it will
be passed directly to the `-c` flag on `sh`, allowing short shell
scripts to be easily run. If `commandargs` are provided in addition,
they will be escaped to ensure they are passed to `shcommand` as
arguments, rather than being interpreted by `sh`.

<dl>
<dt><tt>dockeropts</tt></dt>
<dd>Any arguments accepted by <tt>docker run</tt>.</dd>
<dt><tt>image</tt></dt>
<dd>The name of a Docker image to run</dd>
<dt><tt>shargs</tt></dt>
<dd>Arguments to pass to <tt>sh</tt></dd>
<dt><tt>shcommand</tt></dt>
<dd>An optional command to run via <tt>sh</tt></dd>
<dt><tt>commandargs</tt></dt>
<dd>Any further arguments to pass to <tt>shcommand</tt></dd>
