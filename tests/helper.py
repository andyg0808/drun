from tempfile import TemporaryDirectory

import pytest


def check_run(main, args, expected=0):
    __tracebackhide__ = True
    with TemporaryDirectory() as dir:
        result = main(dir, args)
        if result.returncode != expected:
            if expected == 0:
                pytest.fail(
                    "Command returned non-zero status: {}".format(result.returncode)
                )
            else:
                pytest.fail(
                    "Incorrect exit status: {} != {}".format(
                        expected, result.returncode
                    )
                )
