import os

import pytest
from drun.dsh import main

from .helper import check_run


def test_empty_run():
    with pytest.raises(RuntimeError):
        main(None, [])


def test_only_image_run():
    check_run(main, ["alpine"])


def test_image_run_with_separated_args():
    check_run(main, ["alpine", "echo", "fish"])


def test_image_run_with_combined_args():
    check_run(main, ["alpine", "echo fish && echo frog"])


def test_image_run_with_docker_args():
    check_run(main, ["--env=PASS=true", "alpine", '[ "$PASS" == "true" ]'])


def test_image_fail():
    check_run(main, ["--env=PASS=false", "alpine", '[ "$PASS" == "true" ]'], expected=1)
