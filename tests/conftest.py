from tempfile import TemporaryDirectory

import pytest


@pytest.fixture
def dir():
    with TemporaryDirectory() as dir:
        yield dir
