import os

import pytest
from drun.drun import main

from .helper import check_run


def test_basic():
    with pytest.raises(RuntimeError):
        main(os.getcwd(), [])


def test_basic_image_run():
    check_run(main, ["bash", "bash", "-c", "echo fish"])


def test_image_run_with_args():
    check_run(main, ["alpine", "test", "-e", "/bin/sh"])


def test_image_run_with_docker_args():
    check_run(main, ["--env=PASS=true", "bash", "bash", "-c", '[ "$PASS" == "true" ]'])
