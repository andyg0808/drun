#!/usr/bin/env python3
import os
import sys

from .utils import docker


def main(target_dir, args):
    docker_opts = []
    entrypoint = None
    image = None
    program_opts = []
    for arg in args:
        if arg[0] == "-":
            if entrypoint:
                program_opts.append(arg)
            else:
                docker_opts.append(arg)
        else:
            if not image:
                image = arg
            elif not entrypoint:
                entrypoint = arg
            else:
                program_opts.append(arg)

    if not image:
        help("Missing image")
    if not entrypoint:
        help("Missing entrypoint")
    return docker(entrypoint, image, docker_opts, target_dir, program_opts)


def help(msg):
    raise RuntimeError(
        "{}\nUsage: drun [dockerargs] image entrypoint [entrypointargs]".format(msg)
    )


def run():
    target_dir = os.getcwd()
    sys.exit(main(target_dir, sys.argv[1:]).returncode)


if __name__ == "__main__":
    run()
