import sys
from subprocess import run


def docker(entrypoint, image, docker_opts, target_dir, program_opts):
    args = ["docker", "run", "-i"]
    if sys.stdin.isatty():
        args.append("-t")
    args += [
        "-v" + target_dir + ":/mnt",
        "-w/mnt",
        *docker_opts,
        "--entrypoint",
        entrypoint,
        image,
        *program_opts,
    ]
    return run(args)
