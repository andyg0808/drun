#!/usr/bin/env python3
import os
import shlex
import sys

from .utils import docker


def main(target_dir, args):
    docker_opts = []
    image = None
    shargs = []
    shcommand = []
    for arg in args:
        if arg[0] == "-":
            if not image:
                docker_opts.append(arg)
            elif len(shcommand) == 0:
                shargs.append(arg)
            else:
                shcommand.append(arg)
        else:
            if not image:
                image = arg
            else:
                shcommand.append(arg)

    if not image:
        help("Missing image")

    if len(shcommand) == 0:
        program_opts = [*shargs]
    else:
        if len(shcommand) > 1:
            command = join(shcommand)
        else:
            command = shcommand[0]
        program_opts = [*shargs, "-c", command]
    return docker("sh", image, docker_opts, target_dir, program_opts)


def join(command):
    if "join" in dir(shlex):
        return shlex.join(command)
    else:
        return " ".join(map(shlex.quote, command))


def help(msg):
    raise RuntimeError(
        "{}\nUsage: dsh [dockerargs] image [shargs] [shcommand] [commandargs...]".format(
            msg
        )
    )


def run():
    target_dir = os.getcwd()
    sys.exit(main(target_dir, sys.argv[1:]).returncode)


if __name__ == "__main__":
    run()
